/**
 * Main application controller
 *
 * You can use this controller for your whole app if it is small
 * or you can have separate controllers for each logical section
 *
 */
;(function() {

  angular
    .module('boilerplate')
    .controller('MainController', MainController);

  MainController.$inject = ['LocalStorage', 'QueryService', 'Files'];


  function MainController(LocalStorage, QueryService, Files) {

    // 'controller as' syntax
    var self = this;

    this.loadFile = loadFile;
    this.uploadFile = uploadFile;

    ////////////  function definitions
    function loadFile() {
      var data = Files.parseFile().then(function(result) {
        self.data = result;
      })
    }

    function uploadFile() {
      Files.uploadFile()
      .then(function(result) {
        console.log(result);
      })
      .catch(function(err) {
        console.log(err);
      });
    }
  }


})();
