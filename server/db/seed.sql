﻿﻿DROP TABLE IF EXISTS Record;
DROP TABLE IF EXISTS Files;

CREATE TABLE Files (
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  size INTEGER
);

CREATE TABLE Record (
  ID SERIAL PRIMARY KEY,
  fileID SERIAL REFERENCES Files (ID),
  value1 VARCHAR,
  value2 VARCHAR
);
