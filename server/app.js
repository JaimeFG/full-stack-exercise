const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const router = require('./router');
const path = require('path');
const cors = require('cors');

// Set up the express app
const app = express();
app.use(cors());

// Log requests to the console.
app.use(logger('dev'));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('_build'));

app.use(router);

// Setup a default catch-all route that sends back a welcome message in JSON format.
app.get('/', (req, res) => res.status(200).sendFile(path.join(__dirname,'../_build/index.html')));


module.exports = app;
