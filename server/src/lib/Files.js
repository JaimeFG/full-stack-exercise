var Promise = require('bluebird');
var parse = require('csv-parse');
var async = require('async');
var fs = require('fs');
/** 
 * Lib to process files
*/
module.exports = {

  /**
   * Parse a csv file received
   * @param {*} file 
   */
    parseFile(file) {
      return new Promise(function(resolve, reject) {
        if(!file || !file.path) reject({
          code: 'KO',
          msg: 'File not found'
        });

        var parser = parse({delimiter: ','}, function (err, data) {

          resolve({
            name: file.originalname,
            size: file.size,
            data: data
          });
        });

        fs.createReadStream(file.path).pipe(parser);
      });
    }
}
