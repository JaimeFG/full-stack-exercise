var express = require('express');
var router = express.Router();
var upload = require('multer')({ dest: '/Users/jaime/WebstormProjects/AngularJS-Boilerplate/server/uploads' });
var db = require('./db/controller.js');

router.options("/*", function(req, res, next){
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, Content-Type, Authorization, Content-Length, X-Requested-With');
  res.sendStatus(200);
});

router.get('/api/File', db.getAllFiles);
router.get('/api/File/:id', db.getFile);
router.post('/api/File', upload.single('file') ,db.createFile);

module.exports = router;
