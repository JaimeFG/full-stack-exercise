; (function () {
    'use strict';
    angular
        .module('boilerplate')
        .factory('Files', Files);

    Files.$inject = ['$q', 'QueryService'];
    //////////////// factory
    function Files($q, QueryService) {

        var currentFile = null;

        var service = {
            setFile: setFile,
            getFile: getFile,
            uploadFile: uploadFile,
            parseFile: parseFile
        };

        return service;


        //////////////// definition
        /**
         * Set the current file loaded in the directive
         * @param {*} file 
         */
        function setFile(file) {
            var deferred = $q.defer();
            
            if(!file || file.type !== 'text/csv') 
                deferred.reject({ status: 'KO', msg: "El fichero debe ser CSV"});
            this.currentFile = file;
                deferred.resolve({ status: 'OK', msg: ""});

            return deferred.promise;
        }
        /** 
         * Obtains the current loaded file
        */
        function getFile() {
            return this.currentFile;
        }

        /**
         * Upload to the server the current file received for parameter or
         * the current loaded file if 'file' is not received
         * @param {*} file 
         */
        function uploadFile(file) {
            var deferred = $q.defer();

            QueryService.query('POST', '/File', null, {
                file: file || this.currentFile
            })
            console.log('Subiendo fichero', file);
            deferred.resolve({
                status: 200,
                msg: 'Subida correcta'
            });
            return deferred.promise;
        }

        /** 
         * Parse the current CSV file loaded
        */
        function parseFile() {
            var deferred = $q.defer();

            var data = [];
            if (!this.currentFile) deferred.resolve(data);

            Papa.parse(this.currentFile, {
                complete: function(results) {
                    deferred.resolve(results.data);
                }
            });
            return deferred.promise;
        }
    }


})();
