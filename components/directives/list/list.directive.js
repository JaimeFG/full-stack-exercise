; (function () {

    'use strict';

    /**
     * Directive to upload files
     * @author Jaime
     * @ngdoc  Directive
     *
     * @example
     * <list><list/>
     *
     */
    angular
        .module('boilerplate')
        .directive('list', list);

    function list() {

        // Definition of directive
        var directiveDefinitionObject = {
            restrict: 'E',
            replace: false,
            scope: {
                data: '='
            },
            link: directiveLink,
            templateUrl: 'components/directives/list/list.tpl.html'
        };

        function directiveLink(scope, elem, attrs, ctrl) {
        }
        return directiveDefinitionObject;
    }

})();