# full-stack-engineer-exercise
Ejercicicio de Aplicacion angularJS con servidor node

## 1. Setup
```bash
npm install
```
- Instalar paquetes npm y dependencias de bower
- Tener activa una instancia (por defecto localhost:5432) de postgreSql, y crear una base de datos llamada files
* Se adjunta una semilla (seed.sql) que genera las tablas para el proyecto
* Los csv aceptados debe contener 3 columnas

## 2. Crear bundle de produccion
```bash
npm run build
```
o
```bash
gulp build
```
- Este proceso realiza:
* Limpieza fichero previo _build
* Compila y minifica ficheros de estilos
* copia y optimiza imagenes
* minifica y genera el fichero para acceder con $templateCache
* Construye el nuevo index.html
* minifica y copia los ficheros JS
* copia las fuentes


## 3. Lanzar lado cliente sin bundle (Browsersync)
```bash
npm run server
```
o
```bash
gulp server
```

## 4. Lanzar aplicacion cliente sirviendo el bundle (Browsersync)
```bash
npm run serverbuild
```
o
```bash
gulp server-build
```

## 5. Lanzar aplicacion sirviendo desde backend 
```bash
npm run bundle
```
